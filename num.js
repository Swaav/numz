ONE_TO_TWENTY = ["ten",
  "eleven", "twelve", "thirteen", "fourteen", "fifteen",
  "sixteen", "seventeen", "eighteen", "nineteen", "twenty",
];

one_nine = ["", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"]

TENS = ["", "",
  "twenty", "thirty", "forty", "fifty",
  "sixty", "seventy", "eighty", "ninety"
];


//  SCALES = ["hundred", ];

// helper function for use with Array.filter

function Deuce(number) {
  let num_string = ''
  for (let i = 1; i <= 1000; i++) {
    if (i <= 10) {
      num_string += one_nine[i] + ' '
    } else if (i >= 11 && i <= 20) {
      num_string += ONE_TO_TWENTY[i - 10] + ' '
    } else {
      let digits = String(i)
      if (i < 100) {
        let ten = Number(digits[0])
        let ones = Number(digits[1])
        num_string += TENS[ten] + " " + one_nine[ones] + " "
      } else if (i < 1000) {
        let hundreds = Number(digits[0])
        let ten = Number(digits[1])
        let ones = Number(digits[2])
        num_string += one_nine[hundreds] + " hundred "
        if (ten == 1) {
          num_string += ONE_TO_TWENTY[Number(ones)] + " ";
        }
        else if (ten <= 9) {
          num_string += TENS[ten] + " "
          if (ten <= 10) {
            num_string += one_nine[ones]
          }
        }
      }
      else { num_string += "one thousand" }
    }
    num_string += "--------"
  }
  return num_string
}
console.log(Deuce())

document.write(Deuce())










